import multiprocessing
import time

class MultiThreadManager(multiprocessing.Process):

    def __init__(self, target=None, args=None):
        multiprocessing.Process.__init__(self)
        self.target = target
        self.args = args

    def run(self):
        self.target(*self.args)

def wait_them_finish(threads):
    for thread in threads:
        thread.join()

def sleep_thread(timeSleep = 3):
    time.sleep(timeSleep)

def get_count_running():
    return multiprocessing.active_children()

def get_cpus_number():
    return multiprocessing.cpu_count()

def get_value():
    return multiprocessing.Value('d', 0)