def clean_csv_value(value):
    if value is None:
        return r'\N'
    return str(value).replace('\n', '\\n').replace('"','')