CREATE TABLE IF NOT EXISTS public.actor
(
  id integer NOT NULL,
  first_name character varying(50) NOT NULL,
  last_name character varying(50),
  gender character(1) NOT NULL,
  created_date timestamp without time zone NOT NULL,
  CONSTRAINT actor_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.actor
  OWNER TO postgres;

CREATE INDEX actor_gender_idx
  ON public.actor
  USING btree
  (gender COLLATE pg_catalog."default");


CREATE TABLE IF NOT EXISTS public.movie
(
  id integer NOT NULL,
  name character varying(500),
  year character varying(4),
  rank numeric,
  created_date timestamp without time zone NOT NULL,
  CONSTRAINT movie_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.movie
  OWNER TO postgres;

CREATE INDEX movie_date_part_idx
  ON public.movie
  USING btree
  (date_part('dow'::text, created_date));

CREATE INDEX movie_name_idx
  ON public.movie
  USING btree
  (name COLLATE pg_catalog."default");

CREATE INDEX movie_rank_idx
  ON public.movie
  USING btree
  (rank);

CREATE INDEX movie_year_idx
  ON public.movie
  USING btree
  (year COLLATE pg_catalog."default");

CREATE TABLE IF NOT EXISTS public.role
(
  actor_id integer NOT NULL,
  actor_first_name character varying(50),
  actor_last_name character varying(50),
  actor_gender character(1),
  movie_id integer NOT NULL,
  movie_name character varying(100),
  movie_year character varying(4),
  movie_rank numeric,
  role text,
  created_date timestamp without time zone NOT NULL,
  CONSTRAINT role_pkey PRIMARY KEY (actor_id, movie_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.role
  OWNER TO postgres;

CREATE INDEX role_actor_gender_idx
  ON public.role
  USING btree
  (actor_gender COLLATE pg_catalog."default");

CREATE INDEX role_date_part_idx
  ON public.role
  USING btree
  (date_part('dow'::text, created_date));

CREATE INDEX role_movie_rank_idx
  ON public.role
  USING btree
  (movie_rank);

CREATE INDEX role_movie_year_idx
  ON public.role
  USING btree
  (movie_year COLLATE pg_catalog."default");

CREATE INDEX role_role_idx
  ON public.role
  USING btree
  (role COLLATE pg_catalog."default");