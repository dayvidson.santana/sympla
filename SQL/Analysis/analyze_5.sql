-- 5. Qual ator/atriz participou de mais filmes?
SELECT
	actor.actor,
	COUNT(movie_id) AS total
FROM	(
	SELECT DISTINCT
		actor_id,
		CONCAT(actor_first_name,' ',actor_last_name) as actor,
		movie_id
	FROM role
) actor
GROUP BY actor.actor
ORDER BY 2 DESC
LIMIT 1