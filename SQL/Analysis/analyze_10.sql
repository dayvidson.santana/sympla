-- 10.Qual filme tem mais atores?
SELECT
	movie_name,
 	COUNT(actor_id) as total_actors
FROM public.role
GROUP BY
	movie_name
ORDER BY 2 DESC
LIMIT 1