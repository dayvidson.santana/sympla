-- 2.Qual ano possui a maior quantidade de filmes?
SELECT
	year,
	COUNT(*) AS total
FROM	(
	SELECT
		id as movie_id,
		year as year
	FROM movie
	UNION
	SELECT DISTINCT
		movie_id,
		movie_year
	FROM role
) as movies_by_year
GROUP BY
	year
ORDER BY 2 DESC
LIMIT 1