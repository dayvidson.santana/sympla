-- 8. Qual o total de filmes cadastrados em segundas-feiras?
SELECT
	COUNT(DISTINCT movie_id) AS total_movies_monday
FROM (
	SELECT
		DISTINCT
		id as movie_id,
		CAST(created_date AS DATE) AS created_date
	FROM 	movie
	WHERE 	EXTRACT(DOW FROM created_date) = 1
	UNION
	SELECT DISTINCT
		movie_id,
		CAST(created_date AS DATE) AS created_date
	FROM 	role
	WHERE 	EXTRACT(DOW FROM created_date) = 1
) movies_by_date