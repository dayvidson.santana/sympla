-- 3.De todos os filmes cadastrados, qual a porcentagem desses possui algum valor de nota(rank)?
SELECT
	count(*) AS total_contains_rank,
	total_movies.total_movies,
	CASE total_movies.total_movies
		WHEN 0
		THEN 0
		ELSE CAST(count(*) AS FLOAT) / total_movies.total_movies * 100
	END AS perc
FROM (
	SELECT
		id as movie_id,
		rank as movie_rank
	FROM 	movie
	WHERE	rank IS NOT NULL
	UNION
	SELECT DISTINCT
		movie_id,
		movie_rank
	FROM 	role
	WHERE 	movie_rank is not null
) contains_rank
LEFT JOIN (
	SELECT
		COUNT(*) AS total_movies
	FROM	(
		SELECT DISTINCT movie_id FROM role
		UNION
		SELECT id FROM movie
	) movies
) AS total_movies
ON 1 = 1
GROUP BY
	total_movies.total_movies