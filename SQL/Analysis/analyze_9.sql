-- 9. Qual é a maior nota e a menor?
SELECT
	MAX(movie_rank) as max_rank,
	MIN(movie_rank) as min_movies
FROM	(
	SELECT
		id as movie_id,
		rank as movie_rank
	FROM 	movie
	WHERE	rank IS NOT NULL
	UNION
	SELECT DISTINCT
		movie_id,
		movie_rank
	FROM 	role
	WHERE 	movie_rank is not null
) contains_rank

