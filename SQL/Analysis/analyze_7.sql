-- 7. Quantos atores/atrizes já representaram eles mesmos (“themselves”, “herself”, “himself”)?
SELECT
	COUNT(DISTINCT actor_id) AS total_actors
FROM 	role
WHERE 	role ~* ('himself|themselves|herself')

