-- 11. Qual a média da quantidade de atores por filme ?
SELECT
	ROUND(AVG(total_actors),2) as avg_qtd_actors_by_films
FROM	(
	SELECT
		movie_name,
		COUNT(actor_id) as total_actors
	FROM public.role
	GROUP BY
		movie_name
) movies