-- 4. Qual a nota média dos filmes?
SELECT
	AVG(movie_rank) as media_movies
FROM	(
	SELECT
		id as movie_id,
		rank as movie_rank
	FROM 	movie
	WHERE	rank IS NOT NULL
	UNION
	SELECT DISTINCT
		movie_id,
		movie_rank
	FROM 	role
	WHERE 	movie_rank is not null
) contains_rank