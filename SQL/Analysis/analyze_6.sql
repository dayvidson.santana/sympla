-- 6. Qual é a distribuição (em porcentagem) dos gêneros de todos os atores?
SELECT
	COUNT(id) as qtd_ator,
	gender,
	CASE total_actors.total
		WHEN 0
		THEN 0
		ELSE CAST(COUNT(id) AS FLOAT) / total_actors.total * 100
	END AS perc
FROM	(
	SELECT id, gender FROM actor WHERE gender IS NOT NULL
	UNION
	SELECT DISTINCT actor_id, actor_gender FROM role WHERE actor_gender IS NOT NULL
) as actor_gender
LEFT JOIN (
	SELECT
		COUNT(*) AS total
	FROM	(
		SELECT DISTINCT id as actor_id FROM actor WHERE gender IS NOT NULL
		UNION
		SELECT DISTINCT actor_id FROM role WHERE actor_gender IS NOT NULL
	) total
) total_actors ON
1 = 1
GROUP BY
	gender,
	total_actors.total