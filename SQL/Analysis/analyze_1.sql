-- 1. Quantos filmes, atores e papéis foram cadastrados?
SELECT
	(
		SELECT
			COUNT(*) AS total_movies
		FROM (
			SELECT DISTINCT
				id
			FROM 	movie
			UNION
			SELECT DISTINCT
				movie_id
			FROM 	role
		) as filmes
	) as total_movies,
	(
		SELECT
			COUNT(*) AS total_actors
		FROM (
			SELECT DISTINCT
				id
			FROM 	actor
			UNION
			SELECT DISTINCT
				actor_id
			FROM 	role
		) as actors
	) as total_actors,
	(
		SELECT
			COUNT(role) as total_roles
		FROM 	role
		WHERE 	role IS NOT NULL
	) as total_roles