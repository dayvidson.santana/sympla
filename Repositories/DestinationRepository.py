import os
import requests
import sys

from Connection.PostgreSQLConnection import PostgreSQLConnection
from Services.StringIteratorIO import StringIteratorIO

class PostgreSQLRepository():
    def __init__(self):
        self.connection = PostgreSQLConnection()

    def executeQuery(self, query, insert = False):
        cursor = self.connection.execute(query,[],insert)
        if not insert:
            result = cursor.fetchall()
            cursor.close()
            return result
        else:
            cursor.close()

    def disconnect(self):
        self.connection.disconnect()

    def copy_from(self, iterator, table):
        delimiter = '|'
        size = 8192
        iterator = StringIteratorIO(iterator)
        cursor = self.connection.copy_from(iterator, table, delimiter, size)
        cursor.close()

    def delete(self, table):
        query = "DELETE FROM {};".format(table)
        cursor = self.connection.execute(query)
        rows_deleted = cursor.rowcount
        cursor.close()
        return rows_deleted