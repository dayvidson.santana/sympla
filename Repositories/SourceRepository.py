import urllib.request

class SourceRepository():
    def __init__(self):
        pass

    def getSource(self):
        url = "https://www.dropbox.com/s/k0zf1ps8nvs75n6/output.zip?dl=1"
        response = urllib.request.urlopen(url)
        data = response.read()
        response.close()
        return data