#!/bin/bash

echo "###########################################"
echo "Dependence installer script"
echo "###########################################"

AUTHGET='sudo'

$AUTHGET apt-get update
$AUTHGET apt-get install python3
$AUTHGET apt install postgresql postgresql-contrib
$AUTHGET -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'postgres';"
$AUTHGET -u postgres psql -U postgres -d postgres -a -f SQL/schema.sql
$AUTHGET apt-get install python3-pip -y
$AUTHGET pip3 install --upgrade pip
$AUTHGET pip3 install requests
$AUTHGET pip3 install psycopg2
$AUTHGET pip3 install pylint
$AUTHGET pip3 install urllib3
$AUTHGET pip3 install tabulate