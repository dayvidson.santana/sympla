from Repositories.DestinationRepository import PostgreSQLRepository
from tabulate import tabulate

class ViewAnalysis():
    def __init__(self):
        self.destination_repository = PostgreSQLRepository()
        self.path = './SQL/Analysis/'
        self.tablefmt = 'orgtbl'
        print('\n============ ANÁLISES ============')
        self.analyze_1()
        self.analyze_2()
        self.analyze_3()
        self.analyze_4()
        self.analyze_5()
        self.analyze_6()
        self.analyze_7()
        self.analyze_8()
        self.analyze_9()
        self.analyze_10()
        self.analyze_11()

    def analyze_1(self):
        print('\n1. Quantos filmes, atores e papéis foram cadastrados?\n')
        file = open(self.path + 'analyze_1.sql', 'r')
        query = file.read()
        file.close()
        results = self.destination_repository.executeQuery(query)[0]
        print(tabulate([[results[0],results[1],results[2]]], headers=['Filmes', 'Atores', 'Papéis'], tablefmt=self.tablefmt))

    def analyze_2(self):
        print('\n\n\n2.Qual ano possui a maior quantidade de filmes?\n')
        file = open(self.path + 'analyze_2.sql', 'r')
        query = file.read()
        file.close()
        results = self.destination_repository.executeQuery(query)[0]
        print(tabulate([[results[0],results[1]]], headers=['Ano', 'Total Filmes'], tablefmt=self.tablefmt))

    def analyze_3(self):
        print('\n\n\n3.De todos os filmes cadastrados, qual a porcentagem desses possui algum valor de nota(rank)?\n')
        file = open(self.path + 'analyze_3.sql', 'r')
        query = file.read()
        file.close()
        results = self.destination_repository.executeQuery(query)[0]
        print(tabulate([[results[1],results[2]]], headers=['Total Filmes', 'Percentual'], tablefmt=self.tablefmt))

    def analyze_4(self):
        print('\n\n\n4. Qual a nota média dos filmes?\n')
        file = open(self.path + 'analyze_4.sql', 'r')
        query = file.read()
        file.close()
        results = self.destination_repository.executeQuery(query)[0]
        print(tabulate([[results[0]]], headers=['Nota Média Filmes'], tablefmt=self.tablefmt))

    def analyze_5(self):
        print('\n\n\n5. Qual ator/atriz participou de mais filmes?\n')
        file = open(self.path + 'analyze_5.sql', 'r')
        query = file.read()
        file.close()
        results = self.destination_repository.executeQuery(query)[0]
        print(tabulate([[results[0],results[1]]], headers=['Ator/Atriz','Total Filmes'], tablefmt=self.tablefmt))

    def analyze_6(self):
            print('\n\n\n6. Qual é a distribuição (em porcentagem) dos gêneros de todos os atores?\n')
            file = open(self.path + 'analyze_6.sql', 'r')
            query = file.read()
            file.close()
            results = self.destination_repository.executeQuery(query)
            print(tabulate([[results[0][1],results[0][2]],[results[1][1],results[1][2]]], headers=['Gênero','Percentual'], tablefmt=self.tablefmt))

    def analyze_7(self):
        print('\n\n\n7. Quantos atores/atrizes já representaram eles mesmos (“themselves”, “herself”, “himself”)?\n')
        file = open(self.path + 'analyze_7.sql', 'r')
        query = file.read()
        file.close()
        results = self.destination_repository.executeQuery(query)[0]
        print(tabulate([[results[0]]], headers=['Total Atores/Atrizes'], tablefmt=self.tablefmt))

    def analyze_8(self):
        print('\n\n\n8. Qual o total de filmes cadastrados em segundas-feiras?\n')
        file = open(self.path + 'analyze_8.sql', 'r')
        query = file.read()
        file.close()
        results = self.destination_repository.executeQuery(query)[0]
        print(tabulate([[results[0]]], headers=['Total Filmes'], tablefmt=self.tablefmt))

    def analyze_9(self):
        print('\n\n\n9. Qual é a maior e a menor nota?\n')
        file = open(self.path + 'analyze_9.sql', 'r')
        query = file.read()
        file.close()
        results = self.destination_repository.executeQuery(query)[0]
        print(tabulate([[results[0],results[1]]], headers=['Maior Nota', 'Menor Nota'], tablefmt=self.tablefmt))

    def analyze_10(self):
        print('\n\n\n10.Qual filme tem mais atores?\n')
        file = open(self.path + 'analyze_10.sql', 'r')
        query = file.read()
        file.close()
        results = self.destination_repository.executeQuery(query)[0]
        print(tabulate([[results[0],results[1]]], headers=['Filme', 'Qtd Atores'], tablefmt=self.tablefmt))

    def analyze_11(self):
        print('\n\n\n11.Qual a média da quantidade de atores por filme ?\n')
        file = open(self.path + 'analyze_11.sql', 'r')
        query = file.read()
        file.close()
        results = self.destination_repository.executeQuery(query)[0]
        print(tabulate([[results[0]]], headers=['Média'], tablefmt=self.tablefmt))