import json
import sys
import glob

from math import ceil
from datetime import datetime, date
from Utils.MultiThreadManager import MultiThreadManager, get_count_running, sleep_thread, wait_them_finish, get_value
from Utils.Utils import clean_csv_value
from Repositories.SourceRepository import SourceRepository
from Repositories.DestinationRepository import PostgreSQLRepository
from Services.ViewAnalysis import ViewAnalysis
from zipfile import ZipFile

class ProcessorService():

    def __init__(self, params):
        self.page_size = params.get('pageSize')
        self.threads = params.get('threads')
        self.verbose = params.get('verbose')
        self.analyze = params.get('analyze')

    def start(self):
        """
        This method is the beginning of all processing
        """
        path_file_target = self.__extract() # Extract data from source
        self.__processing_stage(path_file_target)
        if self.analyze:
            ViewAnalysis()

    def __extract(self):
        """
        Extract the information from the informed source, the file is zipped, so I need to unzip it.
        After that, I insert these files in the TXT folder and keep them saved.
        """
        if self.verbose:
            print('Extracting data set\n')
        source_repository = SourceRepository()
        data = source_repository.getSource() # Collecting source data
        path_file_source = './ZIP/'
        path_file_target = './TXT/'
        file_name = 'dataset.zip'
        with open(path_file_source + file_name, "wb") as f: # Saving file in path
            f.write(data)
        with ZipFile(path_file_source + file_name, 'r') as zip_obj: # Create a ZipFile Object and load {file_name} in it
            list_of_file_names = zip_obj.namelist() # Get a list of all archived file names from the zip
            for file_name in list_of_file_names:
                if file_name.startswith('dataset_', 7) and file_name.endswith('txt'): # Check filename startswith dataset_ and endswith txt
                    zip_obj.extract(file_name, 'TXT') # Extract a single file from zip to repository TXT
        return path_file_target

    def __processing_stage(self, path_file_target):
        """
        In this step, the definition of the name of the tables is performed, the deletion of the necessary information and the rest of the processing.
        It is necessary to declare a variable that controls the flow if there is an error within the threads, this variable is called self.validator.
        """

        self.name_table_movie = 'movie'
        self.name_table_actor = 'actor'
        self.name_table_role = 'role'

        self.__delete(self.name_table_movie)
        self.__delete(self.name_table_actor)
        self.__delete(self.name_table_role)

        for fileDir in glob.glob(path_file_target + 'output/*.txt'): # Cycle through existing files
            if self.verbose:
                print('\nProcessing file {}\n'.format(fileDir))
            with open(fileDir, 'r') as file: # Open file in read mode
                data_sets_list = file.readlines() # Stores data in a list
                if data_sets_list:
                    self.validator = get_value()
                    page_size = self.__get_page_size(len(data_sets_list)) # Defines the new page size proportionally, always guaranteeing performance
                    data_set =  self.__pagination(page_size, data_sets_list) # Breaks the list according to page size, serves to define the data that will be delivered to the thread
                    number_of_pages = len(data_set)
                    page = 0
                    threads = []

                    while page < number_of_pages:
                        running = len(get_count_running())
                        if running <= self.threads:
                            if self.verbose:
                                print('Page {} of {}'.format((page+1), number_of_pages))
                            multiThreadManager = MultiThreadManager(target=self.__transform, args=(data_set[page], page+1))
                            threads.append(multiThreadManager)
                            multiThreadManager.start()
                            page += 1
                        else:
                            sleep_thread()

                    if threads:
                        wait_them_finish(threads)

                    if self.validator.value:
                        sys.exit('Error occurred on any thread')

                else:
                    print('Data set does not contain data')

    def __transform(self, data_set, page):
        """
        Transformation is the step that shapes the data in the architecture necessary to carry out the analysis later in the database
        """
        movies_list = {self.name_table_movie: []}
        actors_list = {self.name_table_actor: []}
        roles_list = {self.name_table_role: []}

        try:
            for line in data_set:
                line = json.loads(line)
                created_date = line.get('timestamp')
                if line.get('type') == self.name_table_movie:
                    movie = (
                        int(line.get('id')),
                        line.get('name').replace('"',''),
                        line.get('year'),
                        line.get('rank'),
                        datetime.fromtimestamp(created_date) if created_date else None,
                    )
                    movies_list[self.name_table_movie].append(self.__join(movie)) # Clearing strings so as not to generate error when entering information
                elif line.get('type') == self.name_table_actor:
                    actor = (
                        int(line.get('id')),
                        line.get('first_name'),
                        line.get('last_name'),
                        line.get('gender'),
                        datetime.fromtimestamp(created_date) if created_date else None,
                    )
                    actors_list[self.name_table_actor].append(self.__join(actor)) # Clearing strings so as not to generate error when entering information
                elif line.get('type') == self.name_table_role:
                    role = (
                        line.get('actor',{}).get('id'),
                        line.get('actor',{}).get('first_name'),
                        line.get('actor',{}).get('last_name'),
                        line.get('actor',{}).get('gender'),
                        line.get('movie',{}).get('id'),
                        line.get('movie',{}).get('name'),
                        line.get('movie',{}).get('year'),
                        line.get('movie',{}).get('rank'),
                        line.get('role'),
                        datetime.fromtimestamp(created_date) if created_date else None,
                    )
                    roles_list[self.name_table_role].append(self.__join(role)) # Clearing strings so as not to generate error when entering information
            data_set = {**movies_list, **actors_list, **roles_list}
            self.__load(data_set, page)

        except Exception as error:
            self.validator.value = 1
            print(error)
            print('Error on page {}'.format(page))

    def __load(self, data_set,page):
        """
        In this step the data is inserted in the database
        """
        if data_set:
            for data in data_set:
                destinationRepository = PostgreSQLRepository()
                results_list = data_set.get(data)
                destinationRepository.copy_from(iter(results_list), data)
                if self.verbose:
                    print('Page {} | {} rows inserted in the {} table'.format(page,len(results_list),data))
                destinationRepository.disconnect()

    def __join(self, data_tuple): # Method that clears strings
        return '|'.join(map(clean_csv_value, data_tuple)) + '\n'

    def __get_page_size(self, len_registers):  # Method that recalculates the page size in proportion to the size of the values
        reason = int(ceil(float(len_registers) / self.threads))
        page_size = self.page_size if reason > self.page_size else reason # That way if len_registers is less than the maximum page size, the page is recalculated for maximum processing to occur on threads.
        return page_size

    def __pagination(self, page_size, registers):
        chunk_registers = [registers[i:i+page_size] for i in range(0, len(registers), page_size)]
        return chunk_registers

    def __delete(self, table):
        destination_repository = PostgreSQLRepository()
        rows_deleted = destination_repository.delete(table)
        if self.verbose:
            print('Table {} | {} deleted lines'.format(table, rows_deleted))