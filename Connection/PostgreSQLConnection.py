import os
import sys
import psycopg2

class PostgreSQLConnection():
    connection = None

    def __init__(self):
        connection = self.connect()
        self.cursor = connection.cursor()

    def connect(self):
        try:
            self.connection = psycopg2.connect(
                host = os.environ.get("POSTGRESQL_HOST"),
                port = os.environ.get("POSTGRESQL_PORT"),
                user = os.environ.get("POSTGRESQL_USER"),
                password = os.environ.get("POSTGRESQL_PASSWORD"),
                database = os.environ.get("POSTGRESQL_DATABASE")
            )
            return self.connection
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            sys.exit(1)
        else:
            return self.connection.close()

    def execute(self, query, values = [], requiredCommit = True):
        cursor = self.connection.cursor()
        cursor.execute(query, values)
        if requiredCommit:
            self.connection.commit()
        return cursor

    def copy_from(self, iterator, table, delimiter, size):
        cursor = self.connection.cursor()
        cursor.copy_from(iterator, table, sep=delimiter, size=size)
        self.connection.commit()
        return cursor

    def disconnect(self):
        self.connection.close()
        self.connection = None