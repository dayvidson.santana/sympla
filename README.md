# Teste técnico Sympla
Projeto feito para o teste técnico da empresa Sympla, vaga Engenheiro de Dados
## Pre Requisitos
1. SO Linux
2. Execute o arquivo `install_dependences.sh` como sudo

## Executar Rotina
1. Para rodar a rotina basta executar `./bin/routine` e informar os parametros caso queira. Para saber mais sobre os parametros execute `./bin/routine --help`

## Estratégia
Para fazer essa rotina eu foquei em performance, eu realizo o processamento dos arquivos utilizando multiprocessing, o que da flexibilidade e escalabilidade a nível de produção e consequentemente ganhando tempo, pois os processos estão sendo feitos em paralelo. Padronizei o código para manter o código limpo para entendimento. 

## Decisões Importantes
1. Optei por deixar os arquivos em formato TXT dentro da pasta chamada TXT e o arquivo ZIP dentro da pasta ZIP para caso seja necessário alguma análise antes de inserir os dados no postgreSQL.
2. Optei por utilizar o postgreSQL pois eu gosto do desempenho dele e da facilidade de implementação no python.
3. Após vários estudos, optei por usar o copy_from ao inserir os dados no banco, pois percebi que este método é mais rápido do que qualquer outra forma de inserção na database, para isso preciso transformar a minha lista de dados em um tipo de objeto iterator. Este objeto economiza absurdamente a memória ram, por isso. eu usei ele.
4. Optei por permitir entradas de parametros na rotina para que possa flexibilizar o desempenho dela caso seja necessário sem precisar de subir versão de código.
5. Limitei um valor default a quantidade de threads que seriam processadas simultaneamente, este valor é a quantidade de núcleos do servidor/computador, dessa forma eu não super aloco o servidor/computador que irá rodar a rotina.
6. Coloquei todas as análises feitas em arquivos .sql separados, pois achei que ficaria mais organizado dessa forma.
7. Estou utilizando o arquivo variables.env para configurar as credenciais da conexão do postgreSQL.
8. Precisei criar uma classe chamada StringIteratorIO para realizar a iteração do meu objeto do tipo iterador, dessa forma eu economizo memória ram no momento de inserir dados no postgre.
9. Optei por deletar os dados da tabela durante o processamento, pois se eu fosse realizar upsert perderia performance.
10. Cada tipo de dado eu salvei em tabelas diferentes para facilitar análise.
11. Criei indexes para as querys
