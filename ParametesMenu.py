import getopt
import sys

from Utils.MultiThreadManager import get_cpus_number

def ParametersMenu(argv):
    try:
        number_cpus = get_cpus_number()
        params = {
            'pageSize': 10000,
            'threads': number_cpus,
            'analyze': True, # This parameter is used to display or not the analysis on the screen
            'verbose': True # This parameter is used to display the processing logs on the screen
        }
        options, args = getopt.getopt(argv, 'a:v:mt:p', ['analyze=','help', 'max-threads=', 'page-size=', 'verbose='])

        help_run_routine = """
            How to run the routine
                ./bin/routine parametro1 parametro2
        """

        help_parameters = """
            Valid Parameters
                --analyze or -a (Show analysis at end of processing, valid parameter 1 or 0)
                --verbose or -v (View Logs, valid parameter 1 or 0)
                --page-size or -ps (Page size, valid parameter integer)
                --max-threads or -mt (Maximum threads that can be created, valid parameter integer)
        """

        for opt, arg in options:
            if opt in ('--help'):
                print(help_run_routine + help_parameters)
                sys.exit()
            elif opt in ('-a','--analyze'):
                arg = int(arg)
                if arg > 1:
                    raise NameError('')
                else:
                    params['analyze'] = bool(arg)
            elif opt in ('-v','--verbose'):
                arg = int(arg)
                if arg > 1:
                    raise NameError('')
                else:
                    params['verbose'] = bool(arg)
            elif opt in ('-p','--page-size'):
                params['pageSize'] = int(arg)
            elif opt in ('-mt','--max-threads'):
                params['threads'] = int(arg)

    except Exception:
        error = 'Parameters were not entered correctly\nUse:\n\t--h or --help (For help)'
        print(error)
        sys.exit()

    return params