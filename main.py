import sys

from datetime import datetime
from ParametesMenu import ParametersMenu
from Services.ProcessorService import ProcessorService

def main(argv):
    params = ParametersMenu(argv)
    reportProcessorService = ProcessorService(params)
    reportProcessorService.start()

if __name__ == '__main__':

    dataIni = datetime.now()
    main(sys.argv[1:])

    print('\nTime spent routine: {}'.format(datetime.now() - dataIni))